# qc-python:18:04
# Provides a base Ubuntu (18.04) image with latest Quantconnect Lean installed & configured for Python3

FROM        ubuntu:18.04
MAINTAINER  Robin Coxe <coxe@quanttux.com>

# Last build date - this can be updated whenever there are security updates so
# that everything is rebuilt
ENV         security_updates_as_of 2020-08-25

# This will make apt-get install without question
ARG         DEBIAN_FRONTEND=noninteractive

# Install security updates and required packages
RUN         apt update
RUN         apt -y install -q \
                build-essential \
                ccache \
                git \
                python3-dev \
                python3-pip \
                curl \
                dirmngr \
                gnupg \
                apt-transport-https \
                ca-certificates \
                wget \
                nano 

RUN	     apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN	     sh -c 'echo "deb https://download.mono-project.com/repo/ubuntu stable-bionic main" > /etc/apt/sources.list.d/mono-official-stable.list'
RUN	     apt update
RUN	     apt install -y mono-complete nuget
# Remove all deb package files
RUN          rm -rf /var/lib/apt/lists/*
RUN          mkdir -p /usr/local/src
RUN          git clone https://github.com/QuantConnect/Lean.git /usr/local/src/Lean
WORKDIR      /usr/local/src/Lean
RUN	     nuget restore QuantConnect.Lean.sln
#Build default C# Lean first
RUN          msbuild QuantConnect.Lean.sln
# Install Miniconda Python3
RUN	    wget https://cdn.quantconnect.com/miniconda/Miniconda3-4.5.12-Linux-x86_64.sh
RUN	    bash Miniconda3-4.5.12-Linux-x86_64.sh -b
RUN	    rm -rf Miniconda3-4.5.12-Linux-x86_64.sh
RUN	    $HOME/miniconda3/bin/conda update -y python conda pip 
#Downgrade to Python 3.6.8 because Lean is a picky eater
RUN         $HOME/miniconda3/bin/conda install -y python=3.6.8
RUN         $HOME/miniconda3/bin/conda install -y cython=0.29.11
RUN	    $HOME/miniconda3/bin/conda install -y pandas=0.25.3
RUN         $HOME/miniconda3/bin/conda install -y wrapt=1.11.2
# Note that the Python library softlink command below will FAIL unless it is executed AFTER Python 3.6.8 is installed.
# The Miniconda foo above installs Python 3.7 and 3.8 by default. 
RUN         ln -s $HOME/miniconda3/lib/libpython3.6m.so /usr/lib/libpython3.6m.so
WORKDIR     /usr/local/src/Lean/Launcher
RUN         cp config.json /
WORKDIR     /usr/local/src/Lean
RUN          msbuild QuantConnect.Lean.sln
WORKDIR     /usr/local/src/Lean/Launcher/bin/Debug
RUN         mono QuantConnect.Lean.Launcher.exe
WORKDIR     /usr/local/src/Lean
